package com.example.asynctestapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.asynctestapp.Constants.Constants;
import com.example.asynctestapp.R;
import com.example.asynctestapp.models.Photo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AddPhotoActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = AddPhotoActivity.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;                              // Declaration of the Google Play Services api.
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000; // Request code for sending to Google Play services.
    private LocationRequest mLocationRequest;                              // Object for handling location updates.
    private Location location;

    private EditText editTextName, editTextDescription;

    private Gson gson = new Gson();
    Photo photo = new Photo();

    private ImageView addPhotoImageView;
    private Bitmap bmp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextDescription = (EditText) findViewById(R.id.editTextDescription);

        byte[] byteArray = getIntent().getByteArrayExtra(MainActivity.PHOTO_BITMAP);
        bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        addPhotoImageView = (ImageView) findViewById(R.id.addPhotoImageView);

        addPhotoImageView.setImageBitmap(bmp);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)         // 10 seconds, in milliseconds
                .setFastestInterval(1000);  // 1 second, in milliseconds
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_photo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {

            if( editTextName.getText().toString().trim().equals("") || editTextDescription.getText().toString().trim().equals("")){

                Context context = getApplicationContext();
                CharSequence text = "Please fill in all the fields";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }else{
                generatePhoto();

                PhotoUploadAsync task = new PhotoUploadAsync(bmp);
                task.execute(Constants.REST_URL + "/photo", Constants.REST_URL + "/data");

                Intent intent = new Intent(AddPhotoActivity.this, MainActivity.class);

                startActivity(intent);

                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void generatePhoto() {
        photo.setName(editTextName.getText().toString());

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String cityName = addresses.get(0).getLocality();

            photo.setLocation(cityName);

        } catch (IOException e) {
            e.printStackTrace();
        }

        String rawString = editTextDescription.getText().toString();
        photo.setOriginalName(rawString.replace(" ","_"));

        photo.setDescription(editTextDescription.getText().toString());
        photo.setLongitude(location.getLongitude());
        photo.setLatitude(location.getLatitude());
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");

        Location tempLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (tempLocation == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            location = tempLocation;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");
    }

    @Override
    public void onConnectionFailed(@android.support.annotation.NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(@android.support.annotation.NonNull Location location) {
        this.location = location;
    }

    public class PhotoUploadAsync extends AsyncTask<String, Void, String> {
        private static final String TAG = "PhotoUploadAsync";
        private Bitmap bitmapPhoto;

        public PhotoUploadAsync(Bitmap bitmapPhoto) {
            this.bitmapPhoto = bitmapPhoto;

        }

        @Override
        protected String doInBackground(final String... urls) {
            final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapPhoto.compress(Bitmap.CompressFormat.JPEG, 100, stream);

            Log.d(TAG, "Height: " + bitmapPhoto.getHeight());
            byte[] byteArray = stream.toByteArray();

            OkHttpClient client = new OkHttpClient();

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addPart(
                            Headers.of("Content-Disposition", "form-data; name=\"photo\""),
                            RequestBody.create(MEDIA_TYPE_JPG, byteArray))
                    .build();

            Request request = new Request.Builder()
                    .url(urls[0])
                    .post(requestBody)
                    .build();

            Response response;
            try {
                response = client.newCall(request).execute();
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                photo.setHashName(response.body().string());

            } catch (IOException e) {
                e.printStackTrace();
            }

            String jsonPhoto = gson.toJson(photo);
            //Second post with data
            client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/octet-stream");
            RequestBody body = RequestBody.create(mediaType, jsonPhoto);
            request = new Request.Builder()
                    .url(urls[1])
                    .post(body)
                    .addHeader("cache-control", "no-cache")
                    .build();

            try {
                response = client.newCall(request).execute();
                Log.d(TAG, response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
