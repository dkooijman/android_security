package com.example.asynctestapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asynctestapp.Constants.Constants;
import com.example.asynctestapp.R;
import com.example.asynctestapp.models.Photo;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class DetailsActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView photoNameDetail, locationDetail, detailDetail;
    private Photo photo = new Photo();
    private Context context;

    private static final String TAG = "DetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this.getApplicationContext();

        Intent intent = getIntent();

        int id = intent.getIntExtra(MainActivity.PHOTO_ID_EXTRA, 0);

        DetailsAsyncTask task = new DetailsAsyncTask();
        task.execute(new String[]{Constants.REST_URL + "/photo/"+Integer.toString(id)});
    }

    private void populateFields(Photo photo){
        //Get imageview and load it lazy with Picasso
        imageView = (ImageView) findViewById(R.id.imageViewDetail);

        String url = Constants.REST_URL + "/" + photo.getHashName() + ".jpg";

        Log.d(TAG, url);
        Picasso.with(context)
                .load(url)
                .error(R.mipmap.ic_image_not_found)
                .into(imageView);

        photoNameDetail = (TextView) findViewById(R.id.photoNameDetail);
        locationDetail = (TextView) findViewById(R.id.locationDetail);
        detailDetail = (TextView) findViewById(R.id.detailDetail);

        photoNameDetail.setText(photo.getName());
        locationDetail.setText(photo.getLocation());
        detailDetail.setText(photo.getDescription());
    }

    private class DetailsAsyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialog.show(DetailsActivity.this,
                    "ProgressDialog",
                    "Foto wordt ingeladen");
        }

        @Override
        protected String doInBackground(final String... urls) {

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(urls[0])
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
                JSONObject jsonObject = new JSONObject(response.body().string());

                photo = photo.fromJson(jsonObject);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            populateFields(photo);

            progressDialog.dismiss();
        }
    }

}
