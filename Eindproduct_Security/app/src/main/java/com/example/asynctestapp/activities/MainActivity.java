package com.example.asynctestapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.asynctestapp.Constants.Constants;
import com.example.asynctestapp.R;
import com.example.asynctestapp.models.Photo;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static final String PHOTO_ID_EXTRA = "photoId";
    public static final String PHOTO_BITMAP = "photoBitmap";

    private static final String TAG = "MainActivity";
    private static final int PHOTO_TAKEN_REQUEST = 8008;  // Photo request code

    private Photo[] items;
    ArrayAdapter<Photo> photoListAdapter;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MainAsyncTask task = new MainAsyncTask();
        task.execute(Constants.REST_URL + "/photo");
    }

    private void populateListView() {

        if(items.length == 0){
            Context context = getApplicationContext();
            CharSequence text = "There are no images yet. Start capturing!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }else{
            //Needs to be final for the onClick
            photoListAdapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_list_item_1, items);

            ListView list = (ListView) findViewById(R.id.list);

            list.setAdapter(photoListAdapter);

            //Onclick listener for the photoList
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //Get id of clicked item
                    Photo photo = photoListAdapter.getItem(position);
                    int photoId = photo.getId();

                    Log.d(TAG, "Item: " + photo.getId());

                    Intent intent = new Intent(MainActivity.this, DetailsActivity.class);

                    intent.putExtra(PHOTO_ID_EXTRA, photoId);

                    startActivity(intent);
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == PHOTO_TAKEN_REQUEST) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                super.onActivityResult(requestCode, resultCode, data);
                Bitmap bitmapPhoto = (Bitmap) data.getExtras().get("data");

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                if (bitmapPhoto != null) {
                    bitmapPhoto.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

                    Intent intent = new Intent(MainActivity.this, AddPhotoActivity.class);

                    intent.putExtra(PHOTO_BITMAP, byteArray);

                    startActivity(intent);
                }else {
                    Context context = getApplicationContext();
                    CharSequence text = "Something went wrong!";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_camera) {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, PHOTO_TAKEN_REQUEST);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MainAsyncTask extends AsyncTask<String, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = ProgressDialog.show(MainActivity.this,
                    "ProgressDialog",
                    "Foto's worden geladen!");
        }

        @Override
        protected String doInBackground(final String... urls) {

            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(urls[0])
                    .build();

            Response response;
            try {
                response = client.newCall(request).execute();
                JSONArray jsonArray = new JSONArray(response.body().string());

                ArrayList<Photo> photoList = Photo.fromJson(jsonArray);

                //Create an Array for the photoListAdapter
                items = new Photo[photoList.size()];
                items = photoList.toArray(items);

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            populateListView();

            progressDialog.dismiss();
        }
    }
}