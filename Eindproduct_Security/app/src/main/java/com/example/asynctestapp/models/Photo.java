package com.example.asynctestapp.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by David on 24-12-15.
 */
public class Photo {
    private int id;
    //Name typed in by the user
    private String name;
    private String location;
    //hashed name of the image
    private String hashName;
    //original name of the image
    private String originalName;
    //description typed in by the user
    private String description;
    private double longitude;
    private double latitude;

    public Photo(String name, String location, String hashName, String originalName,
                 String description, double longitude, double latitude) {
        this.name = name;
        this.location = location;
        this.hashName = hashName;
        this.originalName = originalName;
        this.description = description;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Photo() {
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHashName() {
        return hashName;
    }

    public void setHashName(String hashName) {
        this.hashName = hashName;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude){
        this.latitude = latitude;
    }

    // Decodes business json into business model object
    public static Photo fromJson(JSONObject jsonObject) {
        Photo photo = new Photo();

        //Deserialize json into object fields
        try {
            photo.id = jsonObject.getInt("id");
            photo.name = jsonObject.getString("name");
            photo.location = jsonObject.getString("location");
            photo.hashName = jsonObject.getString("hashName");
            photo.originalName = jsonObject.getString("originalName");
            photo.description = jsonObject.getString("description");
            photo.longitude = jsonObject.getDouble("longitude");
            photo.latitude = jsonObject.getDouble("latitude");

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        //Return new object
        return photo;
    }

    // Decodes array of photos json results into photo model objects
    public static ArrayList<Photo> fromJson(JSONArray jsonArray) {
        ArrayList<Photo> photos = new ArrayList<>(jsonArray.length());
        // Process each result in json array, decode and convert to business object
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject photoJson;
            try {
                photoJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            Photo photo = Photo.fromJson(photoJson);
            if (photo != null) {
                photos.add(photo);
            }
        }

        return photos;
    }

    @Override
    public String toString() {
        return this.name + " " + this.description;
    }
}
